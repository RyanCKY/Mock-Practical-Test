
public class User {
	private int userid;
	private String name;
	
	public User (int userid, String name) {
		this.userid = userid;
		this.name = name;
	}
	
	public void setUserid (int userid) {
		this.userid = userid;
	}
	
	public void setName (String name) {
		this.name = name;
	}
	
	public int getUserid () {
		return userid;
	}
	
	public String getName () {
		return name;
	}
	
	public void showBasic () {
		System.out.print("Name of user is " + name + " and User ID is " + userid);
	}
}
