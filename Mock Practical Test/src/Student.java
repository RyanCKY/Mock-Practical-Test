
public class Student extends User{
	private String courseCode;
	private double schoolFee;
	
	public Student(int userid, String name, String courseCode, double schoolFee) {
		super(userid, name);
		this.courseCode = courseCode;
		this.schoolFee = schoolFee;
	}

	public String getCourseCode() {
		return courseCode;
	}

	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	public double getSchoolFee() {
		return schoolFee;
	}

	public void setSchoolFee(double schoolFee) {
		this.schoolFee = schoolFee;
	}
	
	public void show() {
		System.out.println("User ID: " + getUserid());
		System.out.println("Name: " + getName());
		System.out.println("Course code: " + courseCode);
		System.out.println("School Fee: $" + schoolFee);
	}
}
