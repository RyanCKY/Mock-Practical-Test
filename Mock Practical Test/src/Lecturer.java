
public class Lecturer extends User {
	private String roomNo;
	private double salary;
	
	public Lecturer(int userid, String name, String roomNo, double salary) {
		super(userid, name);
		this.roomNo = roomNo;
		this.salary = salary;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	public void show () {
		System.out.println("User ID: " + getUserid());
		System.out.println("Name: " + getName());
		System.out.println("Salary: $" + salary);
		System.out.println("Room No: " + roomNo);
	}
}
