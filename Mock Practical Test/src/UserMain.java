import java.util.ArrayList;

public class UserMain {
	public static double computeAverageSalary (ArrayList<User> userlist) {
		double average = 0;
		
		average += ((Lecturer) userlist.get(2)).getSalary();
		average += ((Lecturer) userlist.get(3)).getSalary();
		
		return average / userlist.size();
	}
	
	public static void main (String args[]) {
		double average = 0;
		
		ArrayList<User> userlist = new ArrayList<User>();
		
		Student s1 = new Student(1001, "Mary Lim", "ITDF01", 1800.0);
		Student s2 = new Student(1002, "Peter Pan", "ITDF02", 1200.0);
		Lecturer e1 = new Lecturer(1003, "John Ang", "#305", 7500.0);
		Lecturer e2 = new Lecturer(1004, "Joe Tay", "#401", 7500.0);
		
		userlist.add(s1);
		userlist.add(s2);
		userlist.add(e1);
		userlist.add(e2);
		
		for (User s3 : userlist) {
			s3.showBasic();
			System.out.print("\n");
		}
		
		average = computeAverageSalary(userlist);
		System.out.println("The average of the lecturer's salary is $" + average);
	}
}
