import java.util.ArrayList;
import java.util.Scanner;

public class UserApp {
	public static void main (String args[]) {
		Scanner in = new Scanner (System.in);
		
		ArrayList<Student> studentlist = new ArrayList<Student>();
		ArrayList<Lecturer> lecturerlist = new ArrayList<Lecturer>();
		ArrayList<User> userlist = new ArrayList<User>();
		
		Student s1;
		Lecturer l1;
		User u0;
		
		int userid = 0;
		String name = "", courseCode = "", roomNo = "";
		double schoolFee;
		int choice = 0, displaychoice;
		double salary = 0;
		
		
		System.out.println("===================");
		System.out.println("|    Main Menu    |");
		System.out.println("===================");
		System.out.println("|   0. Calculate  |");
		System.out.println("|    1. Student   |");
		System.out.println("|    2. Lecturer  |");
		System.out.println("|    3. User      |");
		System.out.println("===================");
		
		do {
			try {
			System.out.print("Enter type of entry: ");
			choice = in.nextInt();
			} catch (Exception e) {
				System.out.print("FATAL ERROR! RESET SYSTEM!");
				System.exit(0);
			}
			
			if (choice != 0) {
				if (choice == 1) {
					
					in.nextLine();
					
					System.out.print("Enter name of Student: ");
					name = in.nextLine();
					
					System.out.print("Enter User ID: ");
					userid = in.nextInt();
					
					System.out.print("Enter Course Code: ");
					courseCode = in.nextLine();
					
					in.nextLine();
					
					System.out.print("Enter School Fees: $");
					schoolFee = in.nextDouble();
					
					s1 = new Student(userid, name, courseCode, schoolFee);
					studentlist.add(s1);
					
					System.out.println(" ");
				}
				else if (choice == 2) {
					
					in.nextLine();
					
					System.out.print("Enter name of Lecturer: ");
					name = in.nextLine();
					
					System.out.print("Enter User ID: ");
					userid = in.nextInt();
					
					System.out.print("Enter Salary: $");
					salary = in.nextDouble();
					
					in.nextLine();
					
					System.out.print("Enter Room Number: ");
					roomNo = in.nextLine();
					
					l1 = new Lecturer(userid, name, roomNo, salary);
					lecturerlist.add(l1);
					
					System.out.println(" ");
				}
				
				else if (choice == 3) {
					in.nextLine();
					
					System.out.print("Enter name of User: ");
					name = in.nextLine();
					
					System.out.print("Enter User ID: ");
					userid = in.nextInt();
					
					u0 = new User(userid, name);
					userlist.add(u0);
					
					System.out.println(" ");
				}
			}
		} while (choice != 0);
		
		System.out.println("===================");
		System.out.println("|   Results Menu  |");
		System.out.println("===================");
		System.out.println("|     1. All      |");
		System.out.println("|    2. Student   |");
		System.out.println("|    3. Lecturer  |");
		System.out.println("|    4. User      |");
		System.out.println("===================");
		
		System.out.print("Enter output choice: ");
		displaychoice = in.nextInt();
		
		System.out.println(" ");
		
		if (displaychoice == 1) {
			for (User u1 : userlist) {
				u1.showBasic();
				System.out.println(" ");
				System.out.println("===================");
			}
			
			for (Student s2 : studentlist) {
				s2.show();
				System.out.println("");
				System.out.println("===================");
			}
			
			for (Lecturer l2 : lecturerlist) {
				l2.show();
				System.out.println("");
				System.out.println("===================");
			}
		}
			else if (displaychoice == 2) {
				for (Student s2 : studentlist) {
					s2.show();
					System.out.println("===================");
				}
			}
			else if (displaychoice == 3) {
				for (Lecturer l2 : lecturerlist) {
					l2.show();
					System.out.println("===================");
				}
			}
			else if (displaychoice == 4) {
				for (User u1 : userlist) {
					u1.showBasic();
					System.out.println("===================");
				}
			}
			
			in.close();
			System.out.print("~END OF PROGRAM~");
		}
	}

